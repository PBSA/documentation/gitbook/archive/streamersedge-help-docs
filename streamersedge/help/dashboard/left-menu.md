# Left Menu

The StreamersEdge left menu is used for searching and filtering challenges.

To open the left menu click on the small right arrow > at the top left of the dashboard.

### Search

To search for a challenge enter the search text in the search box and then either click the magnifying glass or press the _enter_ key.

The following fields can be searched on:

* Challenge name.
* Game.
* Creator's username.

{% hint style="warning" %}
**Note**: The search text must be at least three characters and no more than 100 characters long.
{% endhint %}

Any challenges that match the search text, either partially or exactly, will be shown in a table next to the search.

![](<../../../.gitbook/assets/image (14).png>)

{% hint style="warning" %}
Note: The search text isn't case sensitive.
{% endhint %}

If you click on any of the challenges you'll be redirected to the [**Challenge Details**](../challenges/challenge-details.md) page.

If you click on the View all link you'll be redirected to the [**Challenge Grid**](../challenges/challenge-grid.md) page.

### Filters

To use the challenge filters click on the down arrow next to `Filters` and a list of supported filters will be displayed.

![](<../../../.gitbook/assets/image (8).png>)

Two filters are supported:

#### Live Challenges

Clicking on `Live Challenges` will show all of the challenges that are currently streaming.

#### Popular Challenges

Clicking on `Popular Challenges` will show all of the challenges whether live or not, ordered according to the most popular first. The most popular being those that have the highest total of donations so far.

Clicking on the `RESET` button will clear the results grid.

### Combining Search and Filters

The search and filters options can be used together so that, depending on which filter is selected, the search text only applies to the challenges resulting from that filter.
