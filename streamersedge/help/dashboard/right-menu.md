# Right Menu

The StreamersEdge right menu is used  as a shortcut to common features.

To open the right menu click the  👤 icon at the top right of the screen.

![](<../../../.gitbook/assets/image (13).png>)

The options available from the right menu are:

* Update Profile
* Preferences
* Create Challenge
* Add Funds

Each of these features is covered in it's own section in these help documents.

{% content-ref url="../update-profile.md" %}
[update-profile.md](../update-profile.md)
{% endcontent-ref %}

{% content-ref url="../preferences.md" %}
[preferences.md](../preferences.md)
{% endcontent-ref %}

{% content-ref url="../challenges/creating-a-challenge.md" %}
[creating-a-challenge.md](../challenges/creating-a-challenge.md)
{% endcontent-ref %}

{% content-ref url="../funding-and-redeeming/add-funds.md" %}
[add-funds.md](../funding-and-redeeming/add-funds.md)
{% endcontent-ref %}

