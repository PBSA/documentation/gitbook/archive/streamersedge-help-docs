# Dashboard

The StreamersEdge Dashboard is the first page that you'll be taken to after you log in.

There are several components to the dashboard.

#### Live Challenges

This is a sample of challenges that have already started, ordered by the most recently started first

![](<../../../.gitbook/assets/image (22).png>)

Each challenge shows the following:

* Creator's avatar.
* Challenge name.
* Streaming start date and time.
* Total of donations accumulated so far.

You can scroll through the live challenges by clicking on the left or right arrows.

If you click on the avatar you'll be redirected to that gamer's profile page.

If you click on the challenge itself you'll be redirected to the [Challenge Details Page.](../challenges/challenge-details.md)

#### Recommended Challenges

This is a sample of challenges with the highest total of donations

![](<../../../.gitbook/assets/image (24).png>)

These challenges have all the same features as [Live Challenges](./#live-challenges) with the exception that they haven't yet started streaming.

To see all of the challenges click on the _VIEW MORE_ button and you'll be redirected to the [Challenge Grid.](../challenges/challenge-grid.md)

#### Left Menu

The left menu options are accessed by clicking on the small right arrow > at the top left of the dashboard.

For more information on the left menu options see:

{% content-ref url="left-menu.md" %}
[left-menu.md](left-menu.md)
{% endcontent-ref %}

#### Right Menu

The right menu options are accessed by clicking on the small person icon 👤 to the right of _Log Out._

For more information on the right menu options see:

{% content-ref url="right-menu.md" %}
[right-menu.md](right-menu.md)
{% endcontent-ref %}

