# Donations

Donations are what make challenges competitive, entertaining and profitable!

Whether you're a viewer or gamer, donations are open to all. You can stream your own challenges and donate to others, or just be a viewer and donate on challenges.

## STEP 1 - Select a Challenge

To make a donation first click on the challenge from either the [**Dashboard**](dashboard/) or the [**Challenge Grid.**](challenges/challenge-grid.md)

The Challenge Details screen will be displayed for the selected challenge.

![](<../../.gitbook/assets/image (18).png>)

Next click on the `CONDITIONALLY DONATE` button.

## STEP 2 - Conditionally Donate

{% hint style="warning" %}
**Note**: All donations are _conditional_ donations because if a challenge is lost then all donations are returned to the donators.
{% endhint %}

![](<../../.gitbook/assets/image (30).png>)

Enter an amount to donate and then you're ready to complete the process.

Warning messages will be displayed if you try to donate more than you have in your balance or try to donate on a challenge that's no longer accepting donations.

{% hint style="danger" %}
**Important**: For every donation a nominal fixed fee of $0.02 USD is charged. So for example, if you donate $10 USD to a challenge your account will be debited by $10.02 USD
{% endhint %}

Next click on the `DONATE` button.

## STEP 3 - Share the Challenge

After every successful donation the confirmation screen is displayed which also gives you the opportunity to share this challenge with social media or share a link with your friends.

![](<../../.gitbook/assets/image (20).png>)

The functionality of this screen is exactly the same as the share screen during [**Creating a Challenge.**](challenges/creating-a-challenge.md)
