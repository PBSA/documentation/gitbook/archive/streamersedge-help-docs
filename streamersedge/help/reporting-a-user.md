# Report a User

Unfortunately there are always some bad actors online, and games streaming and viewing is not exempt from attracting these people.

To combat these people from spoiling your enjoyment of StreamersEdge you have the option to report a user.

There is no definitive list of reasons why you should report a user, fundamentally the reasons are the same as any other site where users can interact with each other. For example, all offensive materials whether images, textual or otherwise would be a reason to report a user.

Other reasons would be attempts by a user to ‘game’ the system, defraud other users or not honour donations.

To report a user first click on the user’s profile and click on the `REPORT USER` button to open the Report User screen

![](<../../.gitbook/assets/image (11).png>)

Next select a reason to report the user from the pre-defined options, or add your own reason, and then click `REPORT`
