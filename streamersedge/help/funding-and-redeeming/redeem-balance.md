# Redeem Balance

The Redeem Funds screen is opened by clicking on `Redeem Balance` from the [**right menu**](../dashboard/right-menu.md)**.**

{% hint style="warning" %}
**Note**: At the time of writing money can only be withdrawn ( redeemed) from StreamersEdge through PayPal, and in U.S Dollars (USD), but this will change in future releases.
{% endhint %}

![](<../../../.gitbook/assets/image (19).png>)

To redeem money specify the amount and then click on the `REDEEM` button.

Only USD amounts to a precision of two decimal places and no more than your available balance are allowed.&#x20;

After the `REDEEM` button is clicked you'll be redirected to the PayPal log in screen. From here you can add money to your PayPal account the same way you normally would from other sources.

If the withdrawal is successful, a message will be shown and then you'll be taken back to the page you were on before redeeming funds.

You can cancel at any time by clicking on the `CANCEL` button.

{% hint style="danger" %}
**Important**: Before you can redeem any funds you must set up a PayPal account for redemption. By default there won't be one.
{% endhint %}

### Edit PayPal Account

Your PayPal account can be set, or changed, at any time by clicking on the `Edit Account` link.

When prompted add your PayPal account and then click on the `SAVE` button to return to the Redeem Funds screen.

### Fees

When you add funds there are no fees charged. However, when you redeem money a fee of 5% is charged.

{% hint style="info" %}
**Tip**: You should review the fees associated with any withdrawal before clicking on the `REDEEM` button.
{% endhint %}

![](<../../../.gitbook/assets/image (27).png>)

You will also have to pay all normal PayPal fees associated with their service.

