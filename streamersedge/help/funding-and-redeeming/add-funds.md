# Add Funds

The Add Funds screen is opened by clicking on `Add Funds` from the [**right menu**](../dashboard/right-menu.md).

{% hint style="warning" %}
**Note**: At the time of writing StreamersEdge can only be funded through PayPal and with U.S Dollars (USD), but this will change in future releases.
{% endhint %}



To add funds simply specify the amount and then click on the `PayPal` button.

Only USD amounts to a precision of two decimal places are allowed. For example, 12.95 USD

Once the PayPal button is clicked you'll be redirected to the PayPal log in screen. From here you can withdraw money from your PayPal account the same way you normally would from other sources.

If the funding is successful, a message will be shown and then you'll be taken back to the page you were on before adding funds.

You can cancel at any time by clicking on the `CANCEL` button.

### Fees

There are no fees charged by StreamersEdge when you add funds to your account.

However, you will have to pay any PayPal fees associated with their service.
