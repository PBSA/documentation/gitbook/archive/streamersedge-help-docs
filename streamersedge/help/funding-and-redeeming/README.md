# Funding and Redeeming

Creating challenges and, better still, winning challenges requires donations, and donations require funds.

StreamersEdge has a very simple funding and redemption model, made even easier by supporting PayPal which gives you the convenience of funding your account in USD even if your funds are in a different currency.

Once your account is funded through PayPal then you can make donations to streamers, and if you're a streamer with money that you've collected from donations, then you can withdraw it through PayPal as well.

{% content-ref url="add-funds.md" %}
[add-funds.md](add-funds.md)
{% endcontent-ref %}

{% content-ref url="redeem-balance.md" %}
[redeem-balance.md](redeem-balance.md)
{% endcontent-ref %}



