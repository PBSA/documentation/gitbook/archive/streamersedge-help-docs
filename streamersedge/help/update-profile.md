# Update Profile



The Update Profile screen is opened by clicking on `Update Profile` from the [**right menu**](dashboard/right-menu.md)**.**

There are several updates that can made from this screen; most importantly, it is from this screen that all of your social and game connections are made.

![](<../../.gitbook/assets/image (12).png>)

### Customize Avatar

By default an avatar will be chosen for you and displayed as the profile picture.&#x20;

If you sign in to StreamersEdge using one of the [**Sign-In Partners**](logging-in/#log-in-with-a-streamersedge-sign-in-partner) (Twitch, Youtube and Facebook) has your avatar will be displayed as the profile picture here as well.&#x20;

{% hint style="warning" %}
**Note**: If your account has no profile avatar and you link a social media account that has a profile picture, you should update your StreamersEdge avatar to use the newly linked social media account picture.
{% endhint %}

To change your avatar click on `Customize Avatar,` or the avatar itself and then select a new picture from your computer or mobile application. The picture must be:

* An image file type of either:
  * JPEG/JPG
  * PNG
* No larger that 1Mb

### Email Address

Your email address is automatically populated from the email address that was entered during [**account creation**](creating-an-account.md), or from the email address used for a sign-in partner if this option is used for login.&#x20;

You can change the email address by entering a new email address in this field and then clicking on `SAVE`.&#x20;

An email with the confirmation link will be sent to the new email address. Once the new email address is confirmed, by clicking on the confirmation link in the email, the new email address will come in to effect.

### Connecting Accounts

Connecting your account with other social and game platforms is important because it allows the streaming of games and gives you the ability to share the streaming with other social platforms.

{% hint style="danger" %}
Important: Challenges can't be created unless your account is connected to both Twitch and PUBG
{% endhint %}

To connect an account first click on the `CONNECT` button for the application you wish to connect.

Regardless of which application is chosen a confirmation screen will always appear:

![](<../../.gitbook/assets/image (9).png>)

Click on `YES, LINK MY ACCOUNT` to finish the account linking.

The final step(s) in the process change according to the application that's being connected and are determined by that application rather than by StreamersEdge.

{% hint style="danger" %}
**Important**: If you are new to Twitch and don't have an account, after you've created a new Twitch account and connected it to StreamersEdge there could be a delay of up to 20 minutes to provide the stream details.
{% endhint %}
