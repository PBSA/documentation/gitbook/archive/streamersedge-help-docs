# Forgot Password

The Forgot Password screen is accessed by clicking on the  `Forgot your password?` link on the [**Login**](./) page.

![](<../../../.gitbook/assets/image (4).png>)

To reset a password the user's email address, exactly as entered during account creation must be entered and then click on the `RESET PASSWORD` button.

If the email address is valid then email will be sent to your email address with a link to reset your password. The email is in the format.

> Dear Sir/Madam! \
> You requested a password reset.\
> To specify a new password, click the link below:\
> < _a unique Confirmation Link that must be clicked to activate the account_ >

Enter your email address when asked and then click on the `RESET PASSWORD` button. Next you’ll be sent an email with a link to reset your password.&#x20;

After you click on this link you'll be redirected to the StreamersEdge reset password screen to enter a new password and password confirmation. The same minimum requirements for password creation, as detailed in the [**Creating an Account**](../creating-an-account.md) section, also apply here.

Finally click on the `CHANGE PASSWORD` button.
