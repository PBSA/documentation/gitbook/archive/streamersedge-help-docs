# Logging In

There are two ways to log in to StreamersEdge:

1. Using a StreamersEdge account.
2. Using a StreamersEdge Sign In Partner.

## Log In With a StreamersEdge Account

If you've already created a StreamersEdge account then you're ready to log in.

### Step 1 - Open the Log In Screen.

Click `Log In` on the right of the title bar on the home page of Streamers Edge and you will be taken to the Log In screen.

![](<../../../.gitbook/assets/image (3).png>)

### Step 2 - Log In

If you reach the Log In screen in error because you haven't yet created an account then click on the `REGISTER` link to go to the Create an Account screen. See also:

{% content-ref url="forgot-password.md" %}
[forgot-password.md](forgot-password.md)
{% endcontent-ref %}

On the Log In screen enter your username and password.

![](<../../../.gitbook/assets/image (7).png>)

#### Username

Your username, exactly as set during account creation.

#### Password

Your password, exactly as set during account creation.

#### Sign In

To finish logging in click on the `SIGN IN` button.

If the username and password are correct you will be taken to the StreamersEdge dashboard.&#x20;

If either the username or password are incorrect you will be prompted to re-enter them.

If at any time you're unable to remember your password then it can be reset by clicking on the `Forgot your password?` link. See also:



## Log in with a StreamersEdge Sign In Partner

StreamersEdge Sign in Partners are companies and organizations that have partnered with StreamersEdge to enable their customers to use their online credentials to log in to StreamersEdge.&#x20;

Logging in with a sign in partner is a simple way to use StreamersEdge without the need to create a separate account.

StreamersEdge supports the following Single Sign-On (SSO) platforms:

* Twitch
* Facebook
* YouTube
* PeerplaysGlobal

### Step 1 - Select a Sign In Partner

From the Login screen click on the logo of the sign in partner you want to use.

![](<../../../.gitbook/assets/image (28).png>)

After a sign in partner is selected you'll be redirected to the single sign-on page for that application. The sign in process for each application varies but the basic process is as follows:

* The user will be taken the user to the application's SSO Page
* The user will be asked to login and allow StreamersEdge to see user's account details and other content depending on the application.
* Once the user allows this they'll be logged in to StreamersEdge.

#### Peerplays Global

If you already have a Peerplays account then you can log in to StreamersEdge by using the Peerplays Global sign in partner link as follows:

* Click on the Peerplays Global icon, this will take you to the peerplays global login screen.
* Enter your Peerplays login information. The following fields are required:
  * Username - The username of your Peerplays account.
  * Password - The password of your Peerplays account.

![](https://peerplays.atlassian.net/wiki/download/thumbnails/279773445/XS\_R-12\_Login\_Screen\_R-44\_2%20\(1\).jpg?version=1\&modificationDate=1566493883878\&cacheVersion=1\&api=v2\&width=710\&height=400)

{% hint style="info" %}
To help with logging in using your Peerplays account the following text is displayed:

_"The username you enter is powered by the Peerplays blockchain. If you already have a username at Peerplays, please enter it here to link your Streamers Edge profile too it. If you don't have one, simply proceed to register your account"_
{% endhint %}

Finally, click on the `LOGIN` button. Once your credentials are successfully authenticated, confirming that you already have a Peerplays account, then you'll be automatically logged in and re-directed to the StreamersEdge dashboard.
