# Preferences

The Preferences screen is opened by clicking on `Preferences` from the [**right menu**](dashboard/right-menu.md).

From this screen you can set General and Notification preferences.

![](<../../.gitbook/assets/image (15).png>)

### General

In the general section you can make changes to the time format, toggling between 12 and 24 hour format.

By default the 12 hour format is set, to change this click on the toggle button.

### Notifications

In the notifications section you can chose whether to receive challenge notifications or not by clicking on the appropriate radio button.

To update your preferences click on the `SAVE` button.
