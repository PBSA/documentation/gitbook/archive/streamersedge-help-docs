# Creating an Account

Creating a StreamersEdge account is very straightforward and requires only an email address, username and password. No other personal information is required.

### Step 1 - Open the Create an Account Screen

Click `Sign Up` on the right of the title bar on the home page of StreamersEdge and you will be taken to the Create an Account screen.

![](<../../.gitbook/assets/image (6).png>)

### Step 2 - Create an Account

On the Create an Account screen you must fill in all of the required fields, shown with a red asterisk (\*).

![](<../../.gitbook/assets/image (16).png>)

The required fields are:

#### **Email Address**

Any valid email address. An email with a confirmation link will be sent to this email address for confirmation.

#### **Password**

A unique password that meets the following minimum requirements:

* Length must be between 6 and 60 characters.
* Must contain at least one letter, a number and a special character, from (.@!#$%^\*).
* Must not contain any other special characters.
* Must not contain any spaces

#### Confirm Password

This field is required to ensure that if a password is accidentally typed incorrectly it can be verified before the account is created.

The same minimum requirements as the [**Password**](creating-an-account.md#password) field are used.

#### Password Strength Indicator

For security purposes it's very important that each password not only meets the minimum requirements but also a certain 'strength'. A password could be valid but at the same time easy to be guessed.

The Password Strength Indicator ensures that the password not only meets the minimum requirements but also a high degree of security.

* If the password does not meet the minimum requirements above, it will be labelled as Very Weak.
* Possible password strengths in ascending order are : Very Weak, Weak, Medium, and Strong.

![](<../../.gitbook/assets/image (33).png>)

{% hint style="danger" %}
**Important**: Only passwords of _Medium_ or _Strong_ strength will be accepted.
{% endhint %}

#### Username

The Username is what each StreamersEdge user is known as and the name that will be displayed to other users. Usernames are often aliases but the exact naming is entirely at the user's discretion as long as it meets the following minimum requirements:

* Begins with a letter.
* Ends with a letter.
* Contains only letters, digits or hyphens
* All letters are lowercase.
* Length must be between 3 and 60 characters.

Once all of the required fields are entered, and validated, account creation is completed by clicking on the `REGISTER` button.

### Step 3 - Account Confirmation

After the Register button is clicked a confirmation email is sent to your email address, as entered above. The email is in the format.

> Dear Sir/Madam! Thank you for joining Streamers Edge. In order to access your profile, please follow this link: \<unique confirmation link>
>
> Regards,

After clicking the confirmation link the user will be logged in and taken to the StreamersEdge Home Page.
