# Creating a Challenge

{% hint style="danger" %}
Important. You can't create a challenge unless you have first connected your StreamersEdge account to both your Twitch and PUBG accounts.

For me information on connecting accounts see [**Update Profile.**](../update-profile.md)
{% endhint %}

## Step 1 - Open Create Challenge Screen

To open the Create Challenge screen first click on the 👤icon to show the [**right menu**](../dashboard/right-menu.md) and then select `Create Challenge` from the available options.&#x20;

If `Create Challenge` is disabled this means that you haven't connected both your Twitch and PUBG accounts. See [**Update Profile**](../update-profile.md).

A screen similar to the following will be shown:

![](<../../../.gitbook/assets/image (25).png>)

## Step 2 - Name Your Challenge

On the Create Challenge screen enter a name for your challenge.&#x20;

{% hint style="warning" %}
**Note**: You can enter any name for your challenge but it must be no more than 50 characters long.
{% endhint %}

Next, from the list of game icons, click on the game you want to create the challenge for.

**At the time of writing StreamersEdge only supports PUBG.**

Click on the `NEXT` arrow.

## Step 3 - Set a Start Date & Time

Next step is to enter a start date and time for your challenge. This is the time your challenge becomes live.

![](<../../../.gitbook/assets/image (23).png>)

Using the calendar control add a start date and time.

{% hint style="warning" %}
**Note**: The default date and time will be one hour from the current date and time.
{% endhint %}

You can return to the previous screen by clicking on the `CANCEL` arrow or go to the next screen by clicking on the `NEXT` arrow.

## Step 4 - Add Challenge Conditions

Finally the challenge conditions must be created. These are the conditions by which it will be deemed that a challenge is either won or lost.

![](<../../../.gitbook/assets/image (1).png>)

There are three different challenge conditions that can be used:

#### ResultPlace

This is the match placement of the user at the end of the game.

#### WinTime

This is the time that it takes to win the challenge, in seconds.&#x20;

For example: `WinTime < 10`. Means the user must complete the challenge in less than 10 seconds!

#### Frags

This is a name for kills (used by PUBG). Different games could have different definitions for kills or eliminations.

Next select one of the conditions from the drop down list and then select one of the comparison operators from the next drop down list. Finally add a number to the condition. The number has a different meaning according to the condition, as above.

To add additional conditions click on the `+ADD` button.

{% hint style="warning" %}
**Note**: A maximum of three conditions can be created for any challenge.
{% endhint %}

To delete any condition click on the garbage can button.

You can return to the previous screen by clicking on the `CANCEL` arrow or create the challenge by clicking on the `CREATE CHALLENGE` arrow.

## Step 5 - Challenge Confirmation

Before your challenge is created you must first verify the details from the challenge details confirmation screen.

![](<../../../.gitbook/assets/image (32).png>)

If the details are correct click on the `CONTINUE` button. If they aren't click on the `CANCEL` button to return to the previous screen.

## Step 6 - Share Your Challenge

Now that the challenge is created you have the option to share it.

![](<../../../.gitbook/assets/image (2).png>)

StreamersEdge allows you to share challenges with:

* Facebook
* Twitter
* WhatsApp
* Telegram

To share your challenge click on the `COPY` button next to the link and then send it to anybody that you want to see your challenge.

{% hint style="warning" %}
**Note**: This screen will redirect to the [Dashboard](../dashboard/) after 10 seconds.
{% endhint %}

