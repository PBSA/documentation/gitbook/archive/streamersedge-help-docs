# Challenge Grid

The challenge grid is a very simple way of viewing and filtering all of the StreamersEdge challenges. It is opened by clicking on the `View More` button on the [dashboard](../dashboard/).

![](<../../../.gitbook/assets/image (21).png>)

The challenges are shown one page at a time; additional pages can be scrolled to using the arrows at the bottom of the screen.

Each challenge has the following detail:

* Time to start streaming.
* Total Donations accumulated.
* Challenge Name.
* Game.

### Sorting Challenges

The challenges can be sorted according to any of the options in the drop down list. Available options are:

* Challenges By Name
* Challenges By Game
* Challenges By Start Date
* Challenges By Creation Date
* Challenges By Total Donation
* Active Challenges

![](<../../../.gitbook/assets/image (10).png>)

Once a sort option is selected the challenges in the challenge grid will be sorted according to the selected criteria.

### Search

The search feature on the Challenge Grid works exactly the same as the search feature in the [**left menu**](../dashboard/left-menu.md) and can be combined with whichever sort criteria is selected.
