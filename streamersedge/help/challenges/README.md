# Challenges

Challenges are what makes StreamersEdge different from other streaming platforms.

Not only can a streamer showcase their gameplay, and collect donations in the process, but they can also create their own challenges, bringing a much more competitive experience.

{% content-ref url="creating-a-challenge.md" %}
[creating-a-challenge.md](creating-a-challenge.md)
{% endcontent-ref %}

{% content-ref url="challenge-details.md" %}
[challenge-details.md](challenge-details.md)
{% endcontent-ref %}

{% content-ref url="challenge-grid.md" %}
[challenge-grid.md](challenge-grid.md)
{% endcontent-ref %}





