# Challenge Details

The Challenge Details screen can be displayed when any challenge is selected from the challenge list shown on either the [dashboard](../dashboard/) or from [search](challenge-grid.md#search) results.

![](<../../../.gitbook/assets/image (17).png>)

The following details are shown:

* The challenge name and the game.
* The streamer's username and avatar. On clicking the avatar the streamers profile page will be opened.
* A URL for the live stream. On clicking this link, the Twitch stream will be opened in a new tab.&#x20;
* Previous donations.
* Current bounty.
* Game status - either _Open_, _Live or Resolved._
* Challenge Name.
* Game.

If the challenge is _Open_ or _Live_ you can make a donation by clicking on the `CONDITIONALLY DONATE` button, or share the challenge on social media by clicking on the `SHARE` button.

{% hint style="warning" %}
**Note**: All donations are conditional upon the challenge being completed. Donations are returned to the donators if a challenge is lost.
{% endhint %}

For more information on making donations and sharing challenges see:

{% content-ref url="../../faq/making-conditional-donations.md" %}
[making-conditional-donations.md](../../faq/making-conditional-donations.md)
{% endcontent-ref %}

{% content-ref url="creating-a-challenge.md" %}
[creating-a-challenge.md](creating-a-challenge.md)
{% endcontent-ref %}

