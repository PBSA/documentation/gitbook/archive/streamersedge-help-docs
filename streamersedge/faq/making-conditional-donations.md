# Making Conditional Donations

### **How do I donate?**

You can donate to a challenge by first clicking on the challenge from your dashboard and then clicking on the _Donate_ button.

### **Can I cancel a donation?**

A donation can’t be cancelled but under the following circumstances a donation will be refunded to the donor:

1. The challenge isn’t won.
2. The challenge isn't started or streamed within the stipulated time.
