# Report a User

### **For what reasons should I report a user?**

There is no definitive list of reasons, fundamentally the reasons are the same as any other site where users can interact with others. For example, all offensive materials whether images, textual or otherwise would be a reason to report a user.

Other reasons would be attempts by a user to ‘game’ the system, defraud other users or not honour donations.

### **How do I report a user?**

To report a user first click on the user’s profile and click on the _Report User_ button. Next select a reason to report the user or add your own reason.
