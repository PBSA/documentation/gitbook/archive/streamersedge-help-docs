# Creating an Account

### **How do I create an account?**

To create a new account first click on _Sign Up_ from the top right of the screen, then enter the information on the registration page. All mandatory fields are represented by a red asterisk (\*).

The password strength meter will make sure that you enter a secure password.

Once you’ve entered all the required information click on _Register_. Next you’ll receive an email from StreamersEdge with a link to confirm your registration. Finally click on this link and that’s it, you’re ready to start!

### **Can I have more than one account?**

Yes, you can have multiple accounts provided you use a different email address and username for each account.

### **Will I need to provide any personal information when signing up for a StreamersEdge account?**

No. StreamersEdge does not require any personal information for account creation.
