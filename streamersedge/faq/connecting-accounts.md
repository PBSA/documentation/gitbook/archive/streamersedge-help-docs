# Connecting Accounts

### **Why do I have to connect my Twitch account?**

StreamersEdge edge is about creating, viewing and donating to streaming challenges. As a  streamer in order to participate you must first link your account to Twitch and any games you’re interested in. If you just want to view games, or donate to challenges, you don’t need to connect your account.

### **How do I connect my account?**

You can either connect your account when you first register or connect it later by selecting the game(s) from your Update Profile page. The process to connect your account to a game or social platform could vary according to the application but most times you’ll be asked for a username or nickname.

### **Will my account connections break if I change my StreamersEdge login credentials?**

No. Your StreamersEdge account retains any connected accounts as your account remains your account regardless of whether you username or password changes.

### If I'm new to Twitch, can I get started streaming straight away?

Once you have a new Twitch account connected there could be a delay of up to 20 minutes to provide the stream details. But after that you're good to stream.
