# Log In

### **How do I log in to StreamersEdge?**

To log in to StreamersEdge simply click on Log In from the top right of the screen. Next enter your username and password and finally click on the Sign In button.

### **What is a Sign In Partner?**

StreamersEdge Sign in Partners are companies and organizations that have partnered with StreamersEdge to enable their customers to use their online credentials to log in to StreamersEdge.&#x20;

### **What Sign In Partners are supported?**

StreamersEdge supports Single Sign-On (SSO) with the following platforms:

* Twitch
* Facebook
* YouTube
* Peerplays

### **Do I need to have a Sign In Partner**

No you don’t. Using a Sign in Partner is an easy way to log in to StreamersEdge without needing to set up a new account, but it isn’t mandatory.

### **I’ve forgotten my password, what can I do?**

From the Sign In page first click on the _Forgot your password_ link. Enter your email address when asked and then click on the _Reset Password_ button. Next you’ll be sent an email with a link to reset your password. Click on this link and when requested enter a new password and password confirmation. Finally click on the _Change Password_ button.
