# Privacy



### **What personal information does StreamersEdge hold about me?**

StreamerEdge does not store or hold any personal information about you whatsoever, not even your name.

### **Does StreamersEdge track or store my IP address or any other such data?**

No, StreamersEdge does not store or track user IP addresses, mac addresses or any other such identifying information.
