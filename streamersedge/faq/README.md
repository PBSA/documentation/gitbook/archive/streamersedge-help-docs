# FAQ

{% content-ref url="general.md" %}
[general.md](general.md)
{% endcontent-ref %}

{% content-ref url="creating-an-account.md" %}
[creating-an-account.md](creating-an-account.md)
{% endcontent-ref %}

{% content-ref url="log-in.md" %}
[log-in.md](log-in.md)
{% endcontent-ref %}

{% content-ref url="connecting-accounts.md" %}
[connecting-accounts.md](connecting-accounts.md)
{% endcontent-ref %}

{% content-ref url="creating-challenges.md" %}
[creating-challenges.md](creating-challenges.md)
{% endcontent-ref %}

{% content-ref url="making-conditional-donations.md" %}
[making-conditional-donations.md](making-conditional-donations.md)
{% endcontent-ref %}

{% content-ref url="money.md" %}
[money.md](money.md)
{% endcontent-ref %}

{% content-ref url="report-a-user.md" %}
[report-a-user.md](report-a-user.md)
{% endcontent-ref %}

{% content-ref url="privacy.md" %}
[privacy.md](privacy.md)
{% endcontent-ref %}

{% content-ref url="customer-support.md" %}
[customer-support.md](customer-support.md)
{% endcontent-ref %}

