# Creating Challenges

### **How do I create a challenge?**

To create a challenge open the right menu section by clicking on the small 👤 icon at the right of the title bar. From here select _Create Challenge_.

On the Create Challenge page give your challenge a name and then select a game (currently only PUBG is supported). Next select a start date for the challenge and finally select challenge conditions from the options supported.

{% hint style="warning" %}
**Note**: Only if you have a Twitch and PUBG account connected to StreamersEdge, will you be able to create a challenge. This is required because streaming of a challenge, and playing the game, are a precondition for challenge resolution.
{% endhint %}

### **Why is Create Challenge disabled?**

The Create Challenge option, in the right menu, will be disabled if you haven’t connected both your Twitch and PUBG accounts.

### **How many challenges can I create?**

There is no limit on the number of challenges you can create.

### **How do I win a challenge?**

A challenge is won if you complete all of the challenge conditions that you set out during the challenge creation.

### **What happens to the donations if I lose a challenge?**

If you lose a challenge then all of the donations are returned to the donators.

### **How long does my challenge remain open?**

If you don’t start streaming a challenge within five minutes of the start time that you set for it, or you don’t start playing the game within 30 minutes the challenge will be closed and all donations will be returned to the donators.

### **Can I share my challenges?**

Yes, you can share your challenges. First click on the challenge and then from the Challenge Details page click on the _Share_ button. Next select which application to share the challenge with. The options available are:

* Facebook
* Twitter
* WhatsApp
* Telegram

### **If I win a challenge, how do I get paid?**

If you win a challenge then the total of all the donations you received will be added to your StreamersEdge balance.

### **Are there any delays in challenge resolution?**

There may be a delay of a few minutes before challenge resolution depending on the game being played and how quickly that game broadcasts the result.

### **I’m new to streaming, should I be creating challenges?**

Creating challenges isn’t recommended for novice streamers. StreamersEdge users are unlikely to donate if the streamer has no realistic chance of completing the challenge or the content isn’t appealing to their audience.
