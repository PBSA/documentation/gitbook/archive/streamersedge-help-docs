# General

### What is StreamersEdge?

StreamersEdge is a revolutionary tool that lets broadcasters interact with viewers like they never have before.

### **Is StreamersEdge considered gambling?**

No, StreamersEdge isn’t gambling. Streamers accept donations to complete challenges. All donations are conditional, entirely at the discretion of users and, are returned to the donator if the challenge isn’t created. There is no gambling involved.

### **What broadcasting platforms are supported?**

StreamersEdge supports Twitch as a broadcasting platform, with plans for other platforms to be added in the future.

### **What games are supported?**

Currently StreamersEdge only supports PUBG, but plans are already in place to support many other games in the future.

### **What currencies can I use?**

The only supported currency is US Dollars (USD). However, as PayPal is required to fund your StreamersEdge account then other currencies can be used to first fund your PayPal account.

### **What languages are supported?**

Only English will be supported in the first release.

### **What browsers are supported?**

StreamersEdge supports:

* Google Chrome v70. and higher.
* Firefox v60. and higher.

### **Is there a mobile version of StreamersEdge?**

StreamersEdge can be viewed on most mobile devices, but at present there isn’t a dedicated mobile application. Development is well underway on a version for future release.
