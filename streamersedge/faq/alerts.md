# Alerts

### How do I install alerts?

In OBS, add a new browser source by clicking the small plus sign ( + ) underneath your sources. Add your alerts page URL (see your welcome email) in the URL field and click OK.

Next, right-click on the new browser source and select Filters. Click the small plus sign and select ‘Chroma Key’. Next, select ‘Key Color Type’ and select Custom. Click ‘select color’ and enter #FA8072. Click close and you are all set!

### How do I know if someone donates to my challenge?

Next, right-click on the new browser source and select Filters. Click the small plus sign and select ‘Chroma Key’. Next, select ‘Key Color Type’ and select Custom. Click ‘select color’ and enter #FA8072. Click close and you are all set!

### Can I view all of the donations I've received?

At present there is no way to review a history of donations after it appears on stream - but this functionality will be available soon.\
